import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CostumersService {

  constructor(private db:AngularFirestore) { }


//נגדיר משתנים
costumersCollection:AngularFirestoreCollection;
userCollection:AngularFirestoreCollection = this.db.collection(`users`);


//בלי דפדוף
public getCostumers(userId){ 
  this.costumersCollection = this.db.collection(`users/${userId}/costumers`)
  return this.costumersCollection.snapshotChanges().pipe(map(
    collection => collection.map(
      document => {
        const data = document.payload.doc.data(); 
        data.id = document.payload.doc.id; 
        return data; 
      }
    )
  ))
}



/*

//דפדוף קדימה
  public getCostumers(userId,startAfter){                    
    this.costumersCollection = this.db.collection(`users/${userId}/costumers`, 
    ref => ref.orderBy('name', 'asc').limit(4).startAfter(startAfter)); 
    return this.costumersCollection.snapshotChanges() 
  }

  //דפדוף אחורה
    public getCostumers2(userId,endBefore){                    
      this.costumersCollection = this.db.collection(`users/${userId}/costumers`, 
      ref => ref.orderBy('name', 'asc').limit(4).endBefore(endBefore)); 
      return this.costumersCollection.snapshotChanges() 
    }

*/

deleteCostumer(Userid:string,id:string){ 
  this.db.doc(`users/${Userid}/costumers/${id}`).delete();
}





addCostumer(userId:string, name:string, years:number, income:number){ 
  const costumer = {name:name, years:years, income:income}; 
  this.userCollection.doc(userId).collection('costumers').add(costumer); 
}




updateCostumer(userId:string, id:string, name:string, years:number, income:number){ 
  this.db.doc(`users/${userId}/costumers/${id}`).update( 
    { 
      name:name, 
      years:years,
      income:income
    }
  )
}




  public savePredict(userId, id, returnOrNot){
    this.db.doc(`users/${userId}/costumers/${id}`).update(
      {
        returnOrNot:returnOrNot
      }
    )
  }




}