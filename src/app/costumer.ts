
export interface Costumer {
    id: string,
    name: string,
    years: number,
    income: number, 
    returnOrNot?: string,
}
