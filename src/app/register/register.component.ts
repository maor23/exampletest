import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; //הרצאה 9
//import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'; //תרגיל בית 7
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit { 

  constructor(private authService:AuthService, private router:Router) { }

  //נגדיר משתנים
  hide = true; //תרגיל בית 8 נוסיף משתנה בשביל העין של הסיסמא בטופס
  email:string;
  password:string;
  errorMessage:string; //הרצאה 9
  isError:boolean = false; //הדיפולט יהיה פאלס
  
  
  //הרצאה 9
  //נעביר לקומפוננט כאן במקום מהסרביס את כל נושא הודעות השגיאה והמעבר לבוקס אחרי ההרשמה 
  onSubmit(){ //הרצאה 8 נבנה פונקציה של מה קורה אחרי לחיצת הכפתור שבטופס הרשמה - מצב של הצלחה ומצב של כישלון
    this.authService.SignUp(this.email, this.password)
    .then(res => {   // ה-"רס", זה בעצם התשובה של השרת, שחוזרת לאחר הבקשה של הפרומיס .then במקום סאסקרייב לפרומיס יש את 
          console.log(res); //נדפיס את התשובה לקונסול בשביל דיבאגינג
          this.router.navigate(['/welcome']) //אם ההרשמה בוצעה בהצלחה נפנה את היוזר לקומפוננט בוקס 
        })
      .catch(err => { //בשביל לתפוס הודעות השגיאה
          console.log(err); //נדפיס את התשובה לקונסול בשביל דיבאגינג
          this.isError = true; //יגרום להודעת השגיאה להיות מוצגת, לפי התנאי שרשמנו בטיימפלייט
          this.errorMessage = err.message; //נלכוד את השגיאה עצמה - ל-ארר יש שדה שנקרא מסג
        }) 
    }
  


   ngOnInit(): void {
  }



}







