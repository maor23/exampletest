import { Costumer } from './../costumer';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'costumerform',
  templateUrl: './costumer-form.component.html',
  styleUrls: ['./costumer-form.component.css']
})
export class CostumerFormComponent implements OnInit {

  constructor() { }

  //נגדיר משתנים
  error
  
  @Input() id: string;
  @Input() name: string;
  @Input() years: number;
  @Input() income: number;
  @Input() formType: string;
  
  @Output() update = new EventEmitter<Costumer>(); //האווטפוט הוא על אירוע האפדייט, חובה שאפדייט יהיה מסוג איבנטאמיטר שמוציא אלמנט מסוג בוק - כדי שהבן יוכל לשדר נתונים לאב
  @Output() closeEdit = new EventEmitter<null>();


  updateParent(){
    let costumer:Costumer = {id:this.id, name:this.name, years:this.years, income:this.income}; 
    this.update.emit(costumer); 
    if (this.formType == "Add Costumer"){ 
      this.years = null;
      this.income = null;
    }
  }


  tellParenttoClose(){
    this.closeEdit.emit(); 
  }



  validation(years){
    if (years < '25' && years > '0'){
      this.error = ''
      this.updateParent()
    }
    else{
      this.error = 'Please choose between the years 0-24'
    }
  }



  ngOnInit(): void {
  }

}
