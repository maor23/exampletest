import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Costumers2Component } from './costumers2.component';

describe('Costumers2Component', () => {
  let component: Costumers2Component;
  let fixture: ComponentFixture<Costumers2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Costumers2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Costumers2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
