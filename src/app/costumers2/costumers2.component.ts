import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Costumer2 } from '../costumer2';
import { Costumers2Service } from '../costumers2.service';
import { PredictionService } from '../prediction.service';

@Component({
  selector: 'app-costumers2',
  templateUrl: './costumers2.component.html',
  styleUrls: ['./costumers2.component.css']
})
export class Costumers2Component implements OnInit {

  
  constructor(private customersService:Costumers2Service, public authService:AuthService, private predictionService:PredictionService) { }

//נגדיר משתנים
userId;
customers:Costumer2[];
customers$;
addCustomerFormOpen;
rowToEdit:number = -1; 
customerToEdit:Costumer2 = {name:null, years:null, income:null};
displayedColumns: string[] = ['name', 'Education in years', 'Personal income','Delete', 'Edit', 'Predict', 'Result'];
lastCustomerArrived //דפדוף
firstCustomerArrived;//דפדוף


ngOnInit(): void {

  /*
  //בלי דפדוף
  this.authService.getUser().subscribe(
    user => {
        this.userId = user.uid;
        console.log(user.uid);
        this.customers$ = this.customersService.getCustomers(this.userId);
        this.customers$.subscribe(
          docs => {         
            this.customers = [];
            var i = 0;
            for (let document of docs) {
              console.log(i++); 
              const customer:Costumer2 = document.payload.doc.data();
              if(customer.result){
                customer.saved = true; 
              }
              customer.id = document.payload.doc.id;
                 this.customers.push(customer); 
            }                        
          }
        )
    })
    */


  //עם דפדוף
   this.authService.getUser().subscribe(
    user => {
        this.userId = user.uid;
        console.log(user.uid);
        this.customers$ = this.customersService.getCustomers(this.userId,null);
        this.customers$.subscribe(
          docs => {  
            this.lastCustomerArrived = docs[docs.length-1].payload.doc;       
            this.customers = [];
            var i = 0;
            for (let document of docs) {
              console.log(i++); 
              const customer:Costumer2 = document.payload.doc.data();
              if(customer.result){
                customer.saved = true; 
              }
              customer.id = document.payload.doc.id;
                 this.customers.push(customer); 
            }                        
          }
        )
    })

}





deleteCustomer(index){
  let id = this.customers[index].id;
  this.customersService.deleteCustomer(this.userId, id);
}




add(customer:Costumer2){
  this.customersService.addCustomer(this.userId, customer.name, customer.years, customer.income)
}



updateCustomer(){
  let id = this.customers[this.rowToEdit].id;
  this.customersService.updateCustomer(this.userId, id, this.customerToEdit.name, this.customerToEdit.years, this.customerToEdit.income);
  this.rowToEdit = null;
}




moveToEditState(index){
  console.log(this.customers[index].name);
  this.customerToEdit.name = this.customers[index].name;
  this.customerToEdit.years = this.customers[index].years;
  this.customerToEdit.income = this.customers[index].income;
  this.rowToEdit = index; 
}



updateResult(index){
  this.customers[index].saved = true; 
  this.customersService.updateRsult(this.userId, this.customers[index].id, this.customers[index].result);
}



predict(index){
  this.customers[index].result = 'Will difault';
  this.predictionService.predict(this.customers[index].years, this.customers[index].income).subscribe(
    res => {console.log(res);
      if(res > 0.5){
        var result = 'Will pay';
      } else {
        var result = 'Will default'
      }
      this.customers[index].result = result}
  );  
}




//דפדוף
nextPage(){
  this.customers$ = this.customersService.getCustomers(this.userId, this.lastCustomerArrived); 
  this.customers$.subscribe( 
    docs => { 
      this.lastCustomerArrived = docs[docs.length-1].payload.doc; 
      this.firstCustomerArrived = docs[0].payload.doc; 
      this.customers = []; 
      for(let document of docs){ 
        const book:Costumer2 = document.payload.doc.data(); 
        book.id = document.payload.doc.id; 
        this.customers.push(book); 
      } 
    }
    )
    }



//דפדוף
previewsPage(){
this.customers$ = this.customersService.getCustomers2(this.userId, this.firstCustomerArrived); 
this.customers$.subscribe( 
docs => { 
  this.lastCustomerArrived = docs[docs.length-1].payload.doc; 
  this.firstCustomerArrived = docs[0].payload.doc; 
  this.customers = []; 
  for(let document of docs){ 
    const book:Costumer2 = document.payload.doc.data(); 
    book.id = document.payload.doc.id; 
    this.customers.push(book); 
  } 
}
)
}






}
