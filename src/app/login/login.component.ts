import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router'; //תרגיל בית 9

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  constructor(private authService:AuthService, private router:Router) { } //נייצר הזרקה של הסרביס עבור הקומפוננט של לוגאין

  //נגדיר משתנים
  hide = true; //תרגיל בית 8 נוסיף משתנה בשביל העין של הסיסמא בטופס
  email:string;
  password:string;
  errorMessage:string; //הרצאה 9
  isError:boolean = false; //הדיפולט יהיה פאלס


  //תרגיל בית 9
  // נעביר לקומפוננט כאן במקום מהסרביס את כל נושא הודעות השגיאה והמעבר לבוקס אחרי ההתחברות 
  // נבנה פונקציה של מה קורה אחרי לחיצת הכפתור שבטופס התחברות - מצב של הצלחה ומצב של כישלון
  onSubmit(){ 
    this.authService.login(this.email, this.password)
    .then(res => {  // ה-"רס", זה בעצם התשובה של השרת, שחוזרת לאחר הבקשה של הפרומיס .then במקום סאסקרייב לפרומיס יש את 
          console.log(res); //נדפיס את התשובה לקונסול בשביל דיבאגינג
          this.router.navigate(['/welcome']) //אם הלוג אין בוצע בהצלחה יהיה מעבר לבוקס 
        })
      .catch(err => { //בשביל לתפוס הודעות השגיאה
          console.log(err); //נדפיס את התשובה לקונסול בשביל דיבאגינג
          this.isError = true; //יגרום להודעת השגיאה להיות מוצגת, לפי התנאי שרשמנו בטיימפלייט
          this.errorMessage = err.message; //נלכוד את השגיאה עצמה - ל-ארר יש שדה שנקרא מסג
        }) 
    }

  
  
  ngOnInit(): void {
  }



}
