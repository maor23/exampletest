export interface User { //ניצור אינטרפייס כדי להגדיר את מבנה הנתונים של כל יוזר
    uid:string,
    email?: string | null, //אופציונלי
    photoUrl?: string, //אופציונלי
    displayName?: string //אופציונלי
}
