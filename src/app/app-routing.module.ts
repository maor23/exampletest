import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ByeComponent } from './bye/bye.component';
import { CostumersComponent } from './costumers/costumers.component';
import { Costumers2Component } from './costumers2/costumers2.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent }, 
  { path: 'signup', component: RegisterComponent }, 
  { path: 'costumers', component: CostumersComponent }, 
  { path: 'costumers2', component: Costumers2Component }, 
  { path: 'welcome', component: WelcomeComponent }, 
  { path: 'bye', component: ByeComponent }, 
];





@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
