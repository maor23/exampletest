import { Observable } from 'rxjs'; 
import { Injectable } from '@angular/core';
import{ AngularFireAuth } from '@angular/fire/auth'; 
import { Router } from '@angular/router'; 
import { User } from './user';




@Injectable({
  providedIn: 'root'
})

export class AuthService {


  constructor(private afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState; 
   } 


  user:Observable<User | null>; 
  

  //הרצאה 7 פונקציית התחברות
  //נוסיף פונקציית לוג אין שתקבל אימייל וסיסמא ותבדוק מול פיירבייס
   login(email:string, password:string){ 
    return this.afAuth.signInWithEmailAndPassword(email,password); //פונקצייה שמורה שמחזירה פרומיס שזה דומה לאוברסרבל
  }


  //הרצאה 8 - פונקציית התנתקות
  logout(){
    this.afAuth.signOut().then(
      res => {
        this.router.navigate(['/bye'])
      }
    );  
  }

  // פונקצייה שמחזירה את היוזר שמחובר, ותחזיר נאל אם אין יוזר מחובר
  getUser():Observable <User | null> { //פונקצייה שתחזיר לנו אוברסרבל של יוזר (ולא את היוזר עצמו)
    return this.user;
  }

  //הרצאה 9 פונקציית הרשמה
  //הסרביס אמור רק להחזיר אובסרבול או פרומיס לקומפוננט וזהו לכן את כל הודעות הגיאה והמעבר לבוקס נעביר לקומפוננט
  SignUp(email:string, password:string){
    return this.afAuth.createUserWithEmailAndPassword(email,password); //פונקצייה שמורה שמחזירה פרומיס שזה דומה לאוברסרבל, נחזיר את הפרומיס
  }

  


  
}





  






