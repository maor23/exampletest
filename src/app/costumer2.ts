export interface Costumer2 {
    name: string;
    years: number;
    income: number;
    id?:string;
    saved?:Boolean;
    result?:string; 
}
