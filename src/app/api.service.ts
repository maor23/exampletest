
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }

  //נוסיף משתנים
  private url = "https://tnzmbad8pj.execute-api.us-east-1.amazonaws.com/beta";

  



  
  predict(years:number, income:number){
    let json = {
      "data": 
        {
          "years": years,
          "income": income
        }
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        let final:string = res.body
        return final;       
      })
    )      
  }



}

