import { CostumersService } from './../costumers.service';
import { Costumer } from './../costumer';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-costumers',
  templateUrl: './costumers.component.html',
  styleUrls: ['./costumers.component.css']
})
export class CostumersComponent implements OnInit {

  constructor(private apiService:ApiService, public authService:AuthService, private costumerService:CostumersService) { }

  //נגדיר משתנים
  years:number;
  income:number;
  category:string;
  prob;
  returnOrNot;
  panelOpenState = false;  
  costumers$; 
  userId:string;
  editstate = [];  
  addcostumerFormOpen = false; 

  costumers:Costumer[]; //דפדוף
  lastCostumerArrived; //דפדוף
  firstCostumerArrived; //דפדוף




  ngOnInit(): void {
    //בלי דפדוף
    this.authService.getUser().subscribe( 
    user =>{
      this.userId = user.uid; 
      console.log(this.userId)
      this.costumers$ = this.costumerService.getCostumers(this.userId); 
  }
    )
  }



/*
//דפדוף
this.authService.getUser().subscribe( 
user =>{
  this.userId = user.uid; 
  this.costumers$ = this.costumerService.getCostumers(this.userId,null); 
  this.costumers$.subscribe( 
    docs => { 
      this.lastCostumerArrived = docs[docs.length-1].payload.doc; 
      this.costumers = []; 
      for(let document of docs){ 
        const costumer:Costumer = document.payload.doc.data(); 
        this.costumers.push(costumer); 
      } 
    }
  )
}
)
}
*/

/*
nextPage(){
  this.costumers$ = this.costumerService.getCostumers(this.userId, this.lastCostumerArrived); 
  this.costumers$.subscribe( 
    docs => { 
      this.lastCostumerArrived = docs[docs.length-1].payload.doc; 
      this.firstCostumerArrived = docs[0].payload.doc; 
      this.costumers = []; 
      for(let document of docs){ 
        const costumer:Costumer = document.payload.doc.data(); 
        costumer.id = document.payload.doc.id; 
        this.costumers.push(costumer); 
      } 
    }
  )
}



previewsPage(){
this.costumers$ = this.costumerService.getCostumers2(this.userId, this.firstCostumerArrived); 
this.costumers$.subscribe( 
docs => { 
  this.lastCostumerArrived = docs[docs.length-1].payload.doc; 
  this.firstCostumerArrived = docs[0].payload.doc; 
  this.costumers = []; 
  for(let document of docs){
    const costumer:Costumer = document.payload.doc.data(); 
    costumer.id = document.payload.doc.id; 
    this.costumers.push(costumer); 
  } 
}
)
}
*/



  deleteCostumer(id:string){ 
    this.costumerService.deleteCostumer(this.userId,id); 
  }




  add(costumer:Costumer){
    this.costumerService.addCostumer(this.userId, costumer.name, costumer.years, costumer.income);
  }




  update(costumer:Costumer){
    this.costumerService.updateCostumer(this.userId,costumer.id, costumer.name,costumer.years,costumer.income);
  }




    predict(years, income){
      this.apiService.predict(years, income).subscribe(
        res => { 
          console.log(res ); 
          this.prob = res;
        }
      )
      if(this.prob >= 0.5){
        this.returnOrNot = "yes";
      }
      else{
        this.returnOrNot = "no";
      }
    }


    savePredict(id, returnOrNot){
      this.costumerService.savePredict(this.userId,id,returnOrNot);
    }

}

