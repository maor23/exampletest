import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Costumer2 } from './costumer2';

@Injectable({
  providedIn: 'root'
})
export class Costumers2Service {

  
  constructor(private db: AngularFirestore) { }


//נגדיר משתנים
userCollection:AngularFirestoreCollection = this.db.collection('users');
customersCollection:AngularFirestoreCollection;


/*
//בלי דפדוף
getCustomers(userId): Observable<any[]> {
  this.customersCollection = this.db.collection(`users/${userId}/customers`, 
     ref => ref.limit(10))
  return this.customersCollection.snapshotChanges();    
} 
*/

//דפדוף קדימה
getCustomers(userId,startAfter):Observable<any[]> {
  this.customersCollection = this.db.collection(`users/${userId}/costumers`, 
     ref => ref.orderBy('name', 'asc').limit(4).startAfter(startAfter));
  return this.customersCollection.snapshotChanges();    
}

//דפדוף אחורה
getCustomers2(userId,endBefore):Observable<any[]> {
  this.customersCollection = this.db.collection(`users/${userId}/costumers`, 
     ref => ref.orderBy('name', 'asc').limit(4).endBefore(endBefore));
  return this.customersCollection.snapshotChanges();    
}




deleteCustomer(userId:string, id:string){
  this.db.doc(`users/${userId}/costumers/${id}`).delete();
}




addCustomer(userId:string, name:string, years:number, income:number){
  const customer:Costumer2 = {name:name, years:years, income:income}
  this.userCollection.doc(userId).collection('costumers').add(customer);
} 




updateCustomer(userId:string, id:string, name:string, years:number, income:number){
  this.db.doc(`users/${userId}/costumers/${id}`).update(
    {
      name:name,
      years:years,
      income:income,
      result:null
    }
  )
}



updateRsult(userId:string, id:string, result:string){
  this.db.doc(`users/${userId}/costumers/${id}`).update(
    {
      result:result
    })
  }







}
