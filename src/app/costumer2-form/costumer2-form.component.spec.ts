import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Costumer2FormComponent } from './costumer2-form.component';

describe('Costumer2FormComponent', () => {
  let component: Costumer2FormComponent;
  let fixture: ComponentFixture<Costumer2FormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Costumer2FormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Costumer2FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
