import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Costumer2 } from '../costumer2';

@Component({
  selector: 'customer2form',
  templateUrl: './costumer2-form.component.html',
  styleUrls: ['./costumer2-form.component.css']
})
export class Costumer2FormComponent implements OnInit {


  constructor() { }

  
  //נגדיר משתנים
  error;
  
  @Input() name: string;
  @Input() years: number;
  @Input() id: string;
  @Input() income: number;
  @Input() formType: string;
  
  @Output() update = new EventEmitter<Costumer2>();
  @Output() closeEdit = new EventEmitter<null>();

  buttonText:String = 'Add customer'; 

  
  
  onSubmit(){ 

  }  

  

  updateParent(){
    let customer:Costumer2 = {id:this.id, name:this.name, years:this.years, income:this.income}; 
    this.update.emit(customer);
    if(this.formType == "Add Customer"){
      this.name = null;
      this.years = null;
      this.income = null; 
    }
  }



  tellParentToClose(){
    this.closeEdit.emit();
  }
  

  validation(years){
    if (years < '25' && years > '0'){
      this.error = ''
      this.updateParent()
    }
    else{
      this.error = 'Please choose between the years 0-24'
    }
  }



  ngOnInit(): void {
    if(this.formType == "Add Customer"){
      this.buttonText = 'Add';
    }
  }

}
