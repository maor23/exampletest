// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBcEryNSr3UkRR1JUyEYEg6yONXO1Oye4A",
    authDomain: "exampletest-58d9d.firebaseapp.com",
    projectId: "exampletest-58d9d",
    storageBucket: "exampletest-58d9d.appspot.com",
    messagingSenderId: "1038104998830",
    appId: "1:1038104998830:web:5e6200aee844ce68224e3f"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
